package com.zeliot.news.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.zeliot.news.R
import com.zeliot.news.databinding.ActivityMainBinding
import com.zeliot.news.ui.db.ArticleDatabase
import com.zeliot.news.ui.repository.NewsRepository
import com.zeliot.news.ui.repository.NewsViewModelFactory

class NewsActivity : AppCompatActivity() {
    lateinit var viewModel: NewsViewModel
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
        binding.bottomNavigation.setupWithNavController(navController)

        val repository = NewsRepository(ArticleDatabase.invoke(this))
        val viewModelFactoryProvider = NewsViewModelFactory(repository)
        viewModel = ViewModelProvider(this,viewModelFactoryProvider)
            .get(NewsViewModel::class.java)
    }
}