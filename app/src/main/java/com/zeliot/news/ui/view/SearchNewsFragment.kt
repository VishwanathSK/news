package com.zeliot.news.ui.view

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.zeliot.news.R
import com.zeliot.news.databinding.FragmentBreakingnewsBinding
import com.zeliot.news.databinding.FragmentSearchnewsBinding
import com.zeliot.news.ui.utils.Resource
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SearchNewsFragment: Fragment() {
    lateinit var viewModel: NewsViewModel
    lateinit var binding: FragmentSearchnewsBinding
    lateinit var adapter: NewsAdapter
    private val TAG = SearchNewsFragment::class.java.name

    override fun onAttach(context: Context) {
        Log.d(TAG,"onAttach")
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG,"onCreate")
        super.onCreate(savedInstanceState)
    }

    override fun onStart() {
        Log.d(TAG,"onStart")
        super.onStart()
    }

    override fun onResume() {
        Log.d(TAG,"onResume")
        super.onResume()
    }

    override fun onStop() {
        Log.d(TAG,"onStop")
        super.onStop()
    }

    override fun onPause() {
        Log.d(TAG,"onPause")
        super.onPause()
    }

    override fun onDestroyView() {
        Log.d(TAG,"onDestroyView")
        super.onDestroyView()
    }

    override fun onDestroy() {
        Log.d(TAG,"onDestroy")
        super.onDestroy()
    }

    override fun onDetach() {
        Log.d(TAG,"onDetach")
        super.onDetach()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d(TAG,"onViewCreated")
        super.onViewCreated(view, savedInstanceState)
        viewModel = (activity as NewsActivity).viewModel
        setUpRecycle()
        binding.searchet.addTextChangedListener {
            lifecycleScope.launch{
                delay(5000L)
                it?.let {
                    if(it.toString().isNotEmpty()){
                        viewModel.searchNews(it.toString().trim())
                    }
                }
            }
        }

        viewModel.searchNews.observe(viewLifecycleOwner , Observer { response ->
            when(response){
                is Resource.Success -> {
                    response.data?.let {news ->
                        Log.d("breakn",response.data.status)
                        Log.d("breakn",response.data.articles.toString())
                        adapter.diff.submitList(news.articles)
                    }
                }
            }
        })

        adapter.setOnItemClickListner {
            val bundle = Bundle()?.apply {
                putSerializable("article",it)
            }
            findNavController().navigate(
                R.id.action_searchNewsFragment_to_detailedNewsFragment,
                bundle
            )
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG,"onCreateView")
        binding = FragmentSearchnewsBinding.inflate(
            inflater,
            container,
            false
        )
        return binding.root
    }

    private fun setUpRecycle() {
        adapter = NewsAdapter()
        val llm = LinearLayoutManager(activity)
        llm.orientation = LinearLayoutManager.VERTICAL
        binding.searchnewsrecy.setHasFixedSize(true)

        binding.searchnewsrecy.setLayoutManager(llm)
        binding.searchnewsrecy.adapter = adapter
    }
}