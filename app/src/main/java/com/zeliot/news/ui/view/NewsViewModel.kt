package com.zeliot.news.ui.view

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.zeliot.news.ui.data.Article
import com.zeliot.news.ui.data.NewsResponse
import com.zeliot.news.ui.repository.NewsRepository
import com.zeliot.news.ui.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class NewsViewModel(
    val repository: NewsRepository
) :ViewModel(){


    val breakingNews:MutableLiveData<Resource<NewsResponse>> =
        MutableLiveData()
    val searchNews:MutableLiveData<Resource<NewsResponse>> =
        MutableLiveData()
    val breakingNewsPageNumber = 1

    init {
        getBreakingNews("in")
    }

    fun getBreakingNews(countryCode:String) = viewModelScope.launch {
        breakingNews.postValue(Resource.Loading())
        val responsedeff = async (Dispatchers.IO){
            (repository.getBreakingNews(countryCode,breakingNewsPageNumber))
        }
        val response = responsedeff.await()
        if(response.isSuccessful){
            breakingNews.postValue(Resource.Success(response.body()))
        }else{
            breakingNews.postValue( Resource.Error(message = response.message()))
        }
    }

    fun searchNews(query:String) = viewModelScope.launch {
        searchNews.postValue(Resource.Loading())
        val responsedeff = async (Dispatchers.IO){
            (repository.searchNews(query,breakingNewsPageNumber))
        }
        val response = responsedeff.await()
        if(response.isSuccessful){
            searchNews.postValue(Resource.Success(response.body()))
        }else{
            searchNews.postValue( Resource.Error(message = response.message()))
        }
    }

    fun insertNews(article: Article){
        viewModelScope.launch {
            async(Dispatchers.IO) {  repository.insert(article) }.await()
        }
    }

    fun delete(article: Article){
        viewModelScope.launch {
            async(Dispatchers.IO) {  repository.delete(article) }.await()
        }
    }

    fun getArticles() = repository.getArticles()
}