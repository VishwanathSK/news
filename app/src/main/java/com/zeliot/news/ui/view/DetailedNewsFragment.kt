package com.zeliot.news.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.zeliot.news.R
import com.zeliot.news.databinding.FragmentDetailedBinding

class DetailedNewsFragment: Fragment(R.layout.fragment_detailed) {
    lateinit var viewModel: NewsViewModel
    val arg : DetailedNewsFragmentArgs by navArgs()
    lateinit var binding: FragmentDetailedBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = (activity as NewsActivity).viewModel
        var article = arg.article
        binding.webview.apply {
            webViewClient = WebViewClient()
            article.url?.let { loadUrl(it) }
        }
        binding.fb.setOnClickListener {
            viewModel.insertNews(article)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailedBinding.inflate(
            inflater,
            container,
            false
        )
        return binding.root
    }
}