package com.zeliot.news.ui.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zeliot.news.databinding.AdapterLayoutBinding
import com.zeliot.news.ui.data.Article

class NewsAdapter: RecyclerView.Adapter<NewsAdapter.ArticleViewHolder>() {

    inner class ArticleViewHolder(val binding: AdapterLayoutBinding):RecyclerView.ViewHolder(binding.root){
        fun getBinder() = binding
    }

    private val diffutilcallback = object: DiffUtil.ItemCallback<Article>() {
        override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean {
            return oldItem.url == newItem.url
        }

        override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean {
                return oldItem.urlToImage == newItem.urlToImage
        }
    }

    val diff = AsyncListDiffer(this,diffutilcallback)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        val binding = AdapterLayoutBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ArticleViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        val data = diff.currentList[position]
        holder.getBinder().title.text = data.author
        holder.getBinder().publishedAt.text = data.publishedAt
        holder.getBinder().source.text = data.source?.name
        holder.getBinder().desc.text = data.description
        holder.getBinder().layout.setOnClickListener { onItemClickListner?.let { it(data) } }
        Glide.with(holder.getBinder().articleimage)
            .load(data.urlToImage)
            .into(holder.getBinder().articleimage)
    }

    override fun getItemCount(): Int {
        return diff.currentList.size
    }

    private var onItemClickListner:((Article)->Unit)? = null

    fun setOnItemClickListner(listner : ((Article)->Unit)?){
        onItemClickListner = listner
    }

}