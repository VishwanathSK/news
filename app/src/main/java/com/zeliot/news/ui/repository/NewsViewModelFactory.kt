package com.zeliot.news.ui.repository

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.zeliot.news.ui.view.NewsViewModel

class NewsViewModelFactory(
    val repository: NewsRepository
):ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NewsViewModel(repository) as T
    }
}