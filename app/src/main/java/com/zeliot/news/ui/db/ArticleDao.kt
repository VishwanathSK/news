package com.zeliot.news.ui.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.zeliot.news.ui.data.Article

@Dao
interface ArticleDao {

    @Insert
    fun insert(article: Article):Long

    @Query("select * from Article")
    fun getAllArticle():LiveData<List<Article>>

    @Delete
    fun delete(article: Article)
}