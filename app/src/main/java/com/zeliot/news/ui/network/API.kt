package com.zeliot.news.ui.network

import com.zeliot.news.ui.data.NewsResponse
import com.zeliot.news.ui.utils.Constants
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface API {

    @GET("v2/top-headlines")
    suspend fun getBreakingNews(
        @Query("country")
        countryCode:String = "in",

        @Query("page")
        pageNumber:Int = 1,

        @Query("apiKey")
        apikey:String = Constants.API_KEY
    ):Response<NewsResponse>

    @GET("v2/everything")
    suspend fun searchForNews(
        @Query("q")
        searchQuery:String ,

        @Query("page")
        pageNumber:Int = 1,

        @Query("apiKey")
        apikey:String = Constants.API_KEY
    ) :Response<NewsResponse>
}