package com.zeliot.news.ui.repository

import com.zeliot.news.ui.data.Article
import com.zeliot.news.ui.db.ArticleDatabase
import com.zeliot.news.ui.network.RetrofitInstance

class NewsRepository(
    val db:ArticleDatabase? = null
) {
    suspend fun getBreakingNews(countryCode:String,pageNumber:Int) =
        RetrofitInstance().api.getBreakingNews(
            countryCode,
            pageNumber
        )

    suspend fun searchNews(searchQuery : String,pageNumber:Int) =
        RetrofitInstance().api.searchForNews(
            searchQuery,
            pageNumber
        )

     fun insert(article: Article) {
        db?.getArticleDao()?.insert(article)
    }

    fun delete(article: Article) {
        db?.getArticleDao()?.delete(article)
    }

    fun getArticles() =
        db?.getArticleDao()?.getAllArticle()


}