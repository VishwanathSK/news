package com.zeliot.news.ui.view

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.zeliot.news.R
import com.zeliot.news.databinding.FragmentSavednewsBinding
import com.zeliot.news.databinding.FragmentSearchnewsBinding
import com.zeliot.news.ui.data.Article

class SavedNewsFragment: Fragment() {
    lateinit var viewModel: NewsViewModel
    lateinit var binding: FragmentSavednewsBinding
    lateinit var adapter: NewsAdapter


    private val TAG = SavedNewsFragment::class.java.name

    override fun onAttach(context: Context) {
        Log.d(TAG,"onAttach")
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG,"onCreate")
        super.onCreate(savedInstanceState)
    }

    override fun onStart() {
        Log.d(TAG,"onStart")
        super.onStart()
    }

    override fun onResume() {
        Log.d(TAG,"onResume")
        super.onResume()
    }

    override fun onStop() {
        Log.d(TAG,"onStop")
        super.onStop()
    }

    override fun onPause() {
        Log.d(TAG,"onPause")
        super.onPause()
    }

    override fun onDestroyView() {
        Log.d(TAG,"onDestroyView")
        super.onDestroyView()
    }

    override fun onDestroy() {
        Log.d(TAG,"onDestroy")
        super.onDestroy()
    }

    override fun onDetach() {
        Log.d(TAG,"onDetach")
        super.onDetach()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d(TAG,"onViewCreated")
        super.onViewCreated(view, savedInstanceState)
        viewModel = (activity as NewsActivity).viewModel
        setUpRecycle()
        viewModel.getArticles()?.observe(viewLifecycleOwner, fun(it: List<Article>) {
            adapter.diff.submitList(it)
        })
        val itemTouchHelper = object :ItemTouchHelper.SimpleCallback(
            ItemTouchHelper.UP or ItemTouchHelper.DOWN ,
            ItemTouchHelper.RIGHT or ItemTouchHelper.LEFT
        ){
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return true
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val article = adapter.diff.currentList[viewHolder.adapterPosition]
                viewModel.delete(article)
                Snackbar.make(view,"Article deleted successfully",Snackbar.LENGTH_LONG).apply {
                    setAction("undo"){
                        viewModel.insertNews(article)
                    }
                    show()
                }
            }
        }

        ItemTouchHelper(itemTouchHelper).apply {
            attachToRecyclerView(binding.savednewsRecy)
        }
    }

    private fun setUpRecycle() {
        adapter = NewsAdapter()
        val llm = LinearLayoutManager(activity)
        llm.orientation = LinearLayoutManager.VERTICAL
        binding.savednewsRecy.setHasFixedSize(true)
        binding.savednewsRecy.layoutManager = llm
        binding.savednewsRecy.adapter = adapter
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG,"onCreateView")
        binding = FragmentSavednewsBinding.inflate(
            inflater,
            container,
            false
        )
        return binding.root
    }
}