package com.zeliot.news.ui.data

data class Source(
    val id: Any,
    val name: String
)