package com.zeliot.news.ui.utils

sealed class Resource<T>(
    val data:T?=null,
    val message:String?=null
) {
    class Success<T>(data: T?):Resource<T>(data)
    class Error<T>(data: T?=null,message: String?):Resource<T>(message = message)
    class Loading<T>:Resource<T>()
}